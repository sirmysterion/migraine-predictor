#include <TFT_eSPI.h> // Graphics and font library for ILI9341 driver chip
#include <CircularBuffer.h>

#include "xbm.h"             // Sketch tab header for xbm images
#include "graph.h"           // 

TFT_eSPI tft = TFT_eSPI();            // Invoke library
TFT_eSprite img = TFT_eSprite(&tft);  // Create Sprite object "img" with pointer to "tft" object
                                      // the pointer is used by pushSprite() to push it onto the TFT
TFT_eSprite box1 = TFT_eSprite(&tft);
TFT_eSprite DegSprite = TFT_eSprite(&tft);
TFT_eSprite hPaSprite = TFT_eSprite(&tft);
TFT_eSprite SpriteTime = TFT_eSprite(&tft);
TFT_eSprite brainSprite = TFT_eSprite(&tft);

//CircularBuffer<float,720> temperatureBuffer;

void display_logo() {
  int x = ((tft.width()  - BRAIN2_IMAGE_WIDTH))  / 2;
  int y = ((tft.height() - BRAIN2_IMAGE_HEIGHT)) / 2 + 1;

  // Draw bitmap with top left corner at x,y with foreground only color
  // Bits set to 1 plot as the defined color, bits set to 0 are not plotted
  //              x  y  xbm   xbm width  xbm height  color
  tft.drawXBitmap(x, y, BRAIN2_IMAGE, BRAIN2_IMAGE_WIDTH, BRAIN2_IMAGE_HEIGHT, TFT_WHITE);
}

void display_init() {
  tft.init();
  tft.setRotation(3);
  tft.fillScreen(TFT_BLACK); // Black screen fill
}

void display_clear() {
  tft.fillScreen(TFT_BLACK); 
}

void display_weather() {
  box1.createSprite(100, 70);
  box1.fillScreen(TFT_BLACK);
  box1.drawRect(0,0, box1.width(), box1.height(), TFT_WHITE);
  box1.pushSprite( 5, 5 );
  box1.deleteSprite();
}

void display_temperature(CircularBuffer<float,720> &temperatureBuffer) {
  DegSprite.createSprite(100, 70);
  DegSprite.fillScreen(TFT_BLACK);
  DegSprite.drawRect(0,0, DegSprite.width(), DegSprite.height(), WHITE);

  bool redraw = true;
  int tl, th, ti;
  double ty;

  tl = (int)temperatureBuffer.first();
  th = (int)temperatureBuffer.first();
  ti = (int)temperatureBuffer.size() - 1;

  for (int x = 0; x <= ti ; x += 1) {
    if(tl > (int)temperatureBuffer[x]){
      tl = (int)temperatureBuffer[x];
    }

    if(th < (int)temperatureBuffer[x]){
      th = (int)temperatureBuffer[x];
    }
  }


  for (int x = 0; x <= ti ; x += 1) {
    ty = (int)temperatureBuffer[x];

    //Graph(DegSprite, x, ty, 1, DegSprite.height(), DegSprite.width()-1, DegSprite.height(), 0, 120, 800, tl-1, th+1, 40, RED, BLACK, redraw);    // Temperature Fraction
    Graph(hPaSprite, x, ty, 1, DegSprite.height(), DegSprite.width()-1, DegSprite.height(), 0, 720, 800, tl-1, th+1, 40, RED, BLACK, redraw);    // Temperature Full Graph
    redraw = false;
  }

  DegSprite.setCursor((DegSprite.width()/3), (DegSprite.height()/3), 2);
  
  String TempPrint = String(static_cast<int>(temperatureBuffer[temperatureBuffer.size()-1]));
  TempPrint += " C";
  DegSprite.print(TempPrint);  
  
  //DegSprite.println("Temperature "); //img.setCursor(160, 48); img.print("= "); img.print(temperature); img.print(" C");
  //DegSprite.println("Humidity ");    //img.setCursor(160, 64); img.print("= "); img.print(humidity);    img.print(" %");
  //DegSprite.println("Pressure ");    //img.setCursor(160, 80); img.print("= "); img.print(pressure);    img.print(" hPa");
  //DegSprite.println("Altitude ");    //img.setCursor(160, 96); img.print("= "); img.print(altitude);    img.print(" m");
  //DegSprite.println("6Hr Temperature Diff  ");    //img.setCursor(160, 96); img.print("= "); img.print(altitude);    img.print(" m");
  //DegSprite.println("6Hr Pressure Diff  ");    //img.setCursor(160, 96); img.print("= "); img.print(altitude);    img.print(" m");

  //DegSprite.print("= "); img.print(temperatureBuffer.last()); img.println(" C");
  //DegSprite.print("= "); img.print(humidity);    img.println(" %");
  //DegSprite.print("= "); img.print(pressureBuffer.last());    img.println(" hPa");
  //DegSprite.print("= "); img.print(tempDiff);     img.println(" C");
  //DegSprite.print("= "); img.print(pressureDiff); img.println(" hPa");

  DegSprite.pushSprite( 5, 85 );
  DegSprite.deleteSprite(); 
}

//void display_pressure(double pi, int pl, int ph, CircularBuffer<float,720> &pressureBuffer) {
void display_pressure(CircularBuffer<float,720> &pressureBuffer) {
  hPaSprite.createSprite(100, 70);
  hPaSprite.fillScreen(TFT_BLACK);
  hPaSprite.drawRect(0,0, hPaSprite.width(), hPaSprite.height(), WHITE);

  bool redraw = true;
  //double py, ty, ti, pi;
  int pl, ph, pi;
  double py;

 
  pl = (int)pressureBuffer.first();
  ph = (int)pressureBuffer.first();
  pi = (int)pressureBuffer.size() - 1;

  for (int x = 0; x <= pi ; x += 1) {
    if(ph < (int)pressureBuffer[x]){
      ph = (int)pressureBuffer[x];
    }
    
    if(pl > (int)pressureBuffer[x]){
      pl = (int)pressureBuffer[x];
    }
  }  

  redraw = true;
  for (int x = 0; x <= pi ; x += 1) {
    py = (double)pressureBuffer[x];

    
    //Graph(hPaSprite, x, py, 1, hPaSprite.height(), hPaSprite.width()-1, hPaSprite.height(), 0, 120, 800, pl-1, ph+1, 200, BLUE, BLACK, redraw);  // Pressure Fraction
    Graph(hPaSprite, x, py, 1, hPaSprite.height(), hPaSprite.width()-1, hPaSprite.height(), 0, 720, 800, pl-1, ph+1, 200, BLUE, BLACK, redraw);  // Pressure Full Graph
    redraw = false;
  }

  hPaSprite.setCursor((hPaSprite.width()*0.3), (hPaSprite.height()/3), 2);
  String hPaPrint = String(static_cast<int>(pressureBuffer.last()));
  hPaPrint += " hPa";
  hPaSprite.print(hPaPrint);

  hPaSprite.pushSprite( 5, 165 );
  hPaSprite.deleteSprite(); 
}

void display_brainAlert() {
  Serial.println("Pressure  Diff Trigger");
  
  brainSprite.createSprite(BRAIN_IMAGE_WIDTH, BRAIN_IMAGE_HEIGHT);
  brainSprite.setColorDepth(1);

  int x = ((brainSprite.width()  - BRAIN_IMAGE_WIDTH))  / 2;
  int y = ((brainSprite.height() - BRAIN_IMAGE_HEIGHT)) / 2 + 1;
  brainSprite.drawXBitmap(x, y, BRAIN_IMAGE, BRAIN_IMAGE_WIDTH, BRAIN_IMAGE_HEIGHT, TFT_WHITE);
  
  brainSprite.pushSprite(24, 176);
  brainSprite.deleteSprite();
}

void display_time(String currDate) {
  SpriteTime.createSprite(170, 32);
  SpriteTime.fillScreen(TFT_BLACK);
  SpriteTime.setTextFont(2);

  SpriteTime.print(currDate);

  SpriteTime.pushSprite( 130, 30 );
  SpriteTime.deleteSprite(); 
}