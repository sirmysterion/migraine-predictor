#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

void bme280_setup();
float update_temperature();
float update_pressure();
float update_humidity();
float update_altitude();