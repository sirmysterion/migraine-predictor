#include <TFT_eSPI.h> // Graphics and font library for ILI9341 driver chip
#include <CircularBuffer.h>

#include "xbm.h"             // Sketch tab header for xbm images
#include "graph.h"           // 


void display_init();
void display_logo();
void display_clear();

void display_weather();
//void display_temperature(double ti, int tl, int th, int *&temperatureArr);
//void display_temperature(double ti, int tl, int th, CircularBuffer<float,720> &temperatureBuffer);

void display_temperature(CircularBuffer<float,720> &temperatureBuffer);
void display_pressure(CircularBuffer<float,720> &pressureBuffer);
void display_brainAlert();
void display_time(String currDate);

