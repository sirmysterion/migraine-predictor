#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define SEALEVELPRESSURE_HPA (1013.25)

#define I2C_SDA         4
#define I2C_SCL         5
#define BME280_ADDRESS  0x76

//char PRESSURESHOW[4];// initializing a character of size 4 for showing the  result
//char TEMPARATURESHOW[4];// initializing a character of size 4 for showing the temparature result

bool bme280_found = false;

Adafruit_BME280 bme;                  // Invoke BME280 library

void bme280_setup() {
  Wire.begin(I2C_SDA, I2C_SCL);
  if (!bme.begin(BME280_ADDRESS)) {
    Serial.println("Could not find a valid BME280 sensor, check wiring, address, sensor ID!");
    Serial.print("SensorID was: 0x"); Serial.println(bme.sensorID(),16);
  }
  else {
    bme280_found = true;
  }
}

float update_temperature() {
  float temperature = bme.readTemperature();
  return temperature;
}

float update_pressure() {
  float pressure = bme.readPressure() / 100.0F;
  return pressure;
}

float update_humidity() {
  float humidity = bme.readHumidity();
  return humidity;
}

float update_altitude() {
  float altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
  return altitude;
}