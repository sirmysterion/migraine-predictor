#include <Arduino.h>
#include <stdio.h>
#include <CircularBuffer.h>

#include <time.h>

#include "display.h"
#include "ota.h"
#include "sensors.h"


CircularBuffer<float,720> pressureBuffer;
CircularBuffer<float,720> temperatureBuffer;

float temperature, humidity, pressure, altitude, tempDiff, pressureDiff;

void update_Sensors() {
  humidity = update_humidity();  
  altitude = update_altitude();

  temperature = update_temperature();
  temperatureBuffer.push(update_temperature());

  pressure = update_pressure();
  pressureBuffer.push(update_pressure());

  tempDiff = temperatureBuffer.last() - temperatureBuffer.first();
  pressureDiff = pressureBuffer.last() - pressureBuffer.first();
}

String getDate() {
    //configTime(-6*3600, 0, "50.93.46.92", "0.ca.pool.ntp.org", "pool.ntp.org");
    time_t now = time(nullptr);
    Serial.println("");
    struct tm timeinfo;
    //gmtime_r(&now, &timeinfo);  // UTC Time
    localtime_r(&now, &timeinfo);
    String date = asctime(&timeinfo);


    //int hours, minutes, seconds, day, month, year;
    //struct tm *local = localtime(&now);
    
    //String date = localtime(&now);
 
    //hours = local->tm_hour;         // get hours since midnight (0-23)
    //minutes = local->tm_min;        // get minutes passed after the hour (0-59)
    //seconds = local->tm_sec;        // get seconds passed after a minute (0-59)

    //day = local->tm_mday;            // get day of month (1 to 31)
    //month = local->tm_mon + 1;      // get month of year (0 to 11)
    //year = local->tm_year + 1900;   // get year since 1900
 
    //// print local time
    //if (hours < 12) {    // before midday
    //  Serial.printf("Time is %02d:%02d:%02d am\n", hours, minutes, seconds);
    //}
    //else {    // after midday
    //  Serial.printf("Time is %02d:%02d:%02d pm\n", hours - 12, minutes, seconds);
    //}
    //
    //// print the current date
    //Serial.printf("Date is: %02d/%02d/%d\n", day, month, year);

    return date;
  }

void setup(void) {

  Serial.begin(115200);

  display_init();
  display_logo();

  bme280_setup();
  ota_setup();
  //setClock();
  
  display_clear();
}

void loop() {

  // Query BME Sensor for New Data
  update_Sensors();

  // Update display with Sensor Results
  display_weather();
  display_temperature(temperatureBuffer);
  display_pressure(pressureBuffer);

  if (pressureDiff < -3 ){
    display_brainAlert();
  }

  String currDate = getDate();
  display_time(currDate);


  ota_loop();

  //delay(1000); // 12Min worth, 2Min Seen
  delay(30000); // 6hrs worth, 1h seen
  
}